﻿using UnityEngine;
using System.Collections;

public class Colour : MonoBehaviour {

	public Transform player;
	public float accumColor = 0;
	public GameObject blob;
	public float rad = 5;
    public float decrease = 0.01f;
    public float innerrad = 0;
    
    public int spawnChance = 1;
    public int roll;
    
	Renderer renderer;


	bool tileVis;
	public bool blobCanSpawn;

	// Use this for initialization
	void Start () 
	{
		tileVis = false;
		blobCanSpawn = true;
		renderer = gameObject.GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{


		float dist = Vector3.Distance (gameObject.transform.position, player.position);

		if (dist < rad && dist > innerrad) 
		{
			accumColor = accumColor + 0.03f;
		} 
		else 
		{
			accumColor = accumColor - decrease;
		}

		if (accumColor > 1) 
		{
			accumColor = 1;
		}

		if (accumColor < 0) 
		{
			accumColor = 0;
		}

		if (accumColor > 0) 
		{
			tileVis = true;
		}

		if (accumColor == 0) 
		{
			tileVis = false;
		}
			
		renderer.enabled = tileVis;
		Color myColor = new Vector4(accumColor, accumColor, accumColor, 1);
		renderer.material.color = myColor;

		if (accumColor == 1) 
		{	
		   roll = Random.Range (0, spawnChance);
            
            if (roll > 1)
            {
                blobCanSpawn = false;
            }
          

			if (blobCanSpawn == true) 
				{
					Vector3 pos = gameObject.transform.position;
					Quaternion rot = Quaternion.identity;
					Instantiate (blob, pos, rot);
					blobCanSpawn = false;

					//Invoke ("spawnBlock", 2);
				}

		}

	}
		
}
