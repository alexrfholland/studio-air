﻿using UnityEngine;
using System.Collections;

public class GenerateSmall : MonoBehaviour {

	public seedScript ScriptfromSeeder;
	public GameObject seeder;

	public GameObject obj;
	public float scale1 = 1.2f;
	public float scale2 = 0.9f;



	float locX;
	float locY;
	float locZ;






	// Use this for initialization
	void Start () 
	{


		seeder = transform.parent.gameObject;
		ScriptfromSeeder = seeder.GetComponent<seedScript> ();
		gameObject.transform.parent = transform.root;

		int spawnDir = Random.Range (0, 13);

		if (spawnDir <= 3) 
		{
			locX = 0.6f;
			locY = 0.6f;
			locZ = -0.6f;
		}

		if (spawnDir >= 4 && spawnDir <= 6) 
		{
			locX = -0.6f;
			locY = 0.6f;
			locZ = -0.6f;
		}

		if (spawnDir >7 && spawnDir <= 9) 
		{
			locX = 0.6f;
			locY = 0.6f;
			locZ = 0.6f;
		}

		if (spawnDir == 10) 
		{
			locX = 0.6f;
			locY = -0.6f;
			locZ = -0.6f;
		}

		if (spawnDir == 11) 
		{
			locX = -0.6f;
			locY = -0.6f;
			locZ = -0.6f;
		}

		if (spawnDir == 12) 
		{
			locX = 0.6f;
			locY = -0.6f;
			locZ = 0.6f;
		}


		ScriptfromSeeder.bodyCount += 1;


		
        if (ScriptfromSeeder.bodyCount < ScriptfromSeeder.growCap) 
		{
			Invoke ("Grow", .3f);
		}


		//Invoke ("Die", 3);



	}

	void Grow()
	{
		
			Vector3 dir = new Vector3 (locX, locY, locZ);
			Vector3 pos = gameObject.transform.position + dir/10;
			Quaternion rot = Quaternion.identity;
			GameObject newBlob = Instantiate (obj, pos, rot) as GameObject;
			newBlob.transform.parent = transform.root;
	}

	void Die()
	{
		Destroy (this.gameObject);
	}

}

