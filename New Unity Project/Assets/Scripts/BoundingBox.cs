﻿using UnityEngine;
using System.Collections;

public class BoundingBox : MonoBehaviour {

	public GameObject boundbox;
	public int maxX = 14;
	public int maxY = 20;
	public int maxZ = 14;

	// Use this for initialization
	void Start () 
	{
		int scaleX = Random.Range (8, maxX);
		int scaleY = Random.Range (15, maxY);
		int scaleZ = Random.Range (8, maxZ);

		Vector3 pos = gameObject.transform.position;
		Quaternion rot = Quaternion.identity;
		GameObject go = Instantiate (boundbox, (transform.position + new Vector3 (0, scaleY/2, 0)), transform.rotation) as GameObject;
		go.transform.localScale = new Vector3 (scaleX, scaleY, scaleZ);

	}
	
	// Update is called once per frame
	void Update () {
//some comment	
	}
}
