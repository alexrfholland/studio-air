﻿using UnityEngine;
using System.Collections;

public class RayCastCreate : MonoBehaviour {

	public GameObject obj;
	public float scale1 = 1.2f;
	public float scale2 = 0.9f;



	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit; //output of operation
		int octaLayerMask = 1 << 8;

		if (Input.GetMouseButtonDown (0))
		{
			if (Physics.Raycast (ray, out hit, 100, octaLayerMask)) 
			{

				//Vector3 pos = hit.point;
				if(hit.normal.x == 0 || hit.normal.y == 0 || hit.normal.z == 0)
				{
					Vector3 pos = hit.transform.position + hit.normal*scale1;
					Quaternion rot = Quaternion.identity;
					Instantiate (obj, pos, rot);
				}

				else 
				{
					Vector3 pos = hit.transform.position + hit.normal*scale2;
					Quaternion rot = Quaternion.identity;
					Instantiate (obj, pos, rot);	
				}

				Debug.Log (hit.normal);

					
			}
		}
	}
}
