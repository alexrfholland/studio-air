﻿using UnityEngine;
using System.Collections;

public class Grid2 : MonoBehaviour {

	public GameObject tile;
	int x;
	int y;
	public int rows ;
	public int cols ;

	public float offset = 20;
	float extra;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < rows*cols; i++) 
		{

			if (y % 2 == 0) 
			{
				extra = 2.5f;
			}
			else 
			{
				extra = 0;
			}

			Vector3 pos = new Vector3 (x*5 + extra, 0, y*1.5f);
			Quaternion rot = Quaternion.identity;
			Instantiate (tile, pos, rot);
		
			x++;

			if(x == cols)
			{
				x = 0;
				y++;
			}
		}


	}

	// Update is called once per frame
	void Update () {

	}
}
