﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

	public GameObject tile;
	int x;
	int y;
	public int rows ;
	public int cols ;
	float extra;
    
    public float xspacing = 3;
    public float yspacing = 3;

	// Use this for initialization
	void Start () 
	{
		for (x = 0; x < rows; x++) 
		{
			for (y = 0; y < cols; y++) 
			{
				//if (y % 2 == 0) 
				//{
				//	extra = 2.5f;
				//} else 
				//{
				//	extra = 0;
				//}
				//Vector3 pos = new Vector3 (x*5 + extra, 0, y*1.5f);

				Vector3 pos = new Vector3 (x*xspacing, 0, y*yspacing) + gameObject.transform.position;
                //Vector3 pos = new Vector3 (x*3, 0, y*3);
				Quaternion rot = Quaternion.identity;
				GameObject newObj = Instantiate (tile, pos, rot) as GameObject;
				newObj.transform.parent = gameObject.transform;

			}
		}
			
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
