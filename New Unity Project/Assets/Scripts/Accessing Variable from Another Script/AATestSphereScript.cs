﻿using UnityEngine;
using System.Collections;

public class AATestSphereScript : MonoBehaviour {

	public AATestCubeScript ScriptFromCube;
	public int cubeCountfromScript;
	public GameObject cube;

	// Use this for initialization
	void Start () 
	{
		ScriptFromCube = cube.GetComponent<AATestCubeScript> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		ScriptFromCube.cubeCount += 1;

		cubeCountfromScript = cube.GetComponent<AATestCubeScript>().cubeCount;
		//cubeCountfromScript = ScriptFromCube.cubeCount;

	}
}
