﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour 
{
	public GameObject destroysphere;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("destroy")) 
		{
			Invoke ("DestroySphere", .1f);
		}
	}

	void DestroySphere()
	{
		destroysphere.SetActive (true);
		Invoke ("Die", .1f);
	}

	void Die()
	{
		Destroy (this.gameObject);
	}

}
